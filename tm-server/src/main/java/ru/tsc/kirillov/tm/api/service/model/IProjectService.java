package ru.tsc.kirillov.tm.api.service.model;

import ru.tsc.kirillov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {
}
