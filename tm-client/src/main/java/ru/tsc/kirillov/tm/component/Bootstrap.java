package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.kirillov.tm.api.endpoint.*;
import ru.tsc.kirillov.tm.api.repository.ICommandRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.kirillov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.kirillov.tm.repository.CommandRepository;
import ru.tsc.kirillov.tm.service.CommandService;
import ru.tsc.kirillov.tm.service.LoggerService;
import ru.tsc.kirillov.tm.service.PropertyService;
import ru.tsc.kirillov.tm.service.TokenService;
import ru.tsc.kirillov.tm.util.SystemUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final FileCommand fileCommand = new FileCommand(this);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    {
        @NotNull final String packageCommands = AbstractCommand.class.getPackage().getName();
        @NotNull final Reflections reflections = new Reflections(packageCommands);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz: classes) {
            registry(clazz);
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        registry((AbstractCommand) clazz.newInstance());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void prepareStartup() {
        loggerService.info("** Добро пожаловать в Task Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
        fileCommand.start();
    }

    private void prepareShutdown() {
        loggerService.info("** Task Manager завершил свою работу **");
        fileCommand.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String firstArg = args[0];
        processArgument(firstArg);
        return true;
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void close() {
        System.exit(0);
    }

    private void processCommand() {
        try {
            System.out.println("\nВведите команду:");
            @NotNull final String cmdText = TerminalUtil.nextLine();
            processCommand(cmdText);
            System.out.println("[Ок]");
            loggerService.command(cmdText);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[Ошибка]");
        }
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) close();
        prepareStartup();
        while (true) processCommand();
    }

}
