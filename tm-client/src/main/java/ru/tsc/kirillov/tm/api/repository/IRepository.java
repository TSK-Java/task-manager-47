package ru.tsc.kirillov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModelDTO> {

    @Nullable
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @NotNull
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M remove(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    @Nullable
    M removeById(@NotNull String id);

    @NotNull
    M removeByIndex(@NotNull Integer index);

    long count();

}
